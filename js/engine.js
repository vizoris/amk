
$(function() {

// Фиксированная шапка припрокрутке


$(window).scroll(function(){
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
  });


// Плавающий плейсхолдер
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();


// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


$(".banner-scroll").on("click", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top}, 1500);
  });


// FansyBox
 $('.fancybox').fancybox({});





$('.section-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1
      },
      
    }
  ]
});




$('.uav-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1
});






// Открыть меню в футере на мобильном
if ( $(window).width() <= 991) {
  $('.footer-links .h4').click(function() {
   $(this).parent('.footer-links').find('ul').slideToggle();
  })

}







})